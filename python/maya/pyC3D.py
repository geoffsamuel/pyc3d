import math

from maya import cmds

from pyBase.maya import utils as mayaUtils
from PyC3D import c3dReader, vector3


class C3DReader(object):
    def __init__(self, filePath=None, launchUI=True):
        super(C3DReader, self).__init__()
        self.allMarkers = []
        if filePath is not None:
            self.loadFile(filePath)
            return
        if launchUI:
            # TODO: Add UI
            pass

    def loadFile(
                self,
                filePath,
                startFrame=None,
                endFrame=None,
                zeroStart=None
                ):
        self.fileData = c3dReader.C3DReader(filePath)

        sceneFrameRate = mayaUtils.currentTimeUnitToFPS()

        self.frameRate = self.fileData.getSampleRate()
        self.skipFrame = (self.frameRate / sceneFrameRate)
        self.startFrame = (sceneFrameRate / self.frameRate) * self.fileData.getFrameStart()
        self.endFrame = (sceneFrameRate / self.frameRate) * self.fileData.getFrameEnd()

        cmds.playbackOptions(minTime=self.startFrame, maxTime=self.endFrame)

        tempMakers = self.fileData.getMarkers()
        self.allMarkers = []
        cmds.spaceLocator(p=[0, 0, 0], name="C3DOpticalRoot")
        for _marker in tempMakers:
            self.allMarkers.append(_marker.replace(":", "_"))
        self.__createMarkers()
        self.__mapFrameData()

    def createCube(self, scale=0.2):
        temp = cmds.curve(d=1,
                            p=[
                                (1, 1, 1), (1, 1, -1), (-1, 1, -1), (-1, 1, 1) , (1, 1, 1),
                                (1, -1, 1), (1, -1, -1), (-1, -1, -1), (-1, -1, 1) , (1, -1, 1),
                                (1, -1, -1), (1, -1, 1), (1, 1, 1) , (1, 1, -1), (1, -1, -1),
                                (-1, -1, -1), (-1, -1, 1), (-1, 1, 1), (-1, 1, -1), (-1, -1, -1)
                            ]
                        )
        cmds.setAttr("%s.scaleX" % temp, scale)
        cmds.setAttr("%s.scaleY" % temp, scale)
        cmds.setAttr("%s.scaleZ" % temp, scale)
        return temp

    def __createMarkers(self):
        for _marker in self.allMarkers:
            # "Making %r" % _marker
            Temp = self.createCube()
            cmds.rename(Temp, _marker)
            cmds.parent(_marker, "C3DOpticalRoot")

    def __mapFrameData(self, zeroStart=None):
        currentFrame = zeroStart or self.startFrame
        frameJump = int(math.ceil(self.skipFrame))
        for data in self.fileData.iterFrame(iterJump=frameJump):
            for idx in xrange(len(self.allMarkers)):
                cmds.setKeyframe(self.allMarkers[idx], v=data[idx].x / 100, time=currentFrame, at='translateX')
                cmds.setKeyframe(self.allMarkers[idx], v=data[idx].y / 100, time=currentFrame, at='translateZ')
                cmds.setKeyframe(self.allMarkers[idx], v=data[idx].z / 100, time=currentFrame, at='translateY')
            currentFrame += 1
