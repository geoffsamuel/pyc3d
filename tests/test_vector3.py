import unittest
from PyC3D import c3dReader, vector3


class Test_Vector3(unittest.TestCase):
    def test_setConstructorValues(self):
        vect = vector3.Vector3(10, 20, 30)
        self.assertEquals(vect.x, 10)
        self.assertEquals(vect.y, 20)
        self.assertEquals(vect.z, 30)

    def test_setterX(self):
        vect = vector3.Vector3(0, 0, 0)
        vect.setX(1.0)
        self.assertEquals(vect.x, 1.0)

    def test_setterY(self):
        vect = vector3.Vector3(0, 0, 0)
        vect.setY(1.0)
        self.assertEquals(vect.y, 1.0)

    def test_setterZ(self):
        vect = vector3.Vector3(0, 0, 0)
        vect.setZ(1.0)
        self.assertEquals(vect.z, 1.0)

class Test_Vector3Setter(unittest.TestCase):
    def test_convertsionInt(self):
        vect = vector3.Vector3(0, 0, 0)
        vect.setX(1)
        self.assertEquals(vect.x, 1.0)
        vect.setY(2)
        self.assertEquals(vect.y, 2.0)
        vect.setZ(3)
        self.assertEquals(vect.z, 3.0)

    def test_convertsionLong(self):
        vect = vector3.Vector3(0, 0, 0)
        vect.setX(1l)
        self.assertEquals(vect.x, 1.0)
        vect.setY(2l)
        self.assertEquals(vect.y, 2.0)
        vect.setZ(3l)
        self.assertEquals(vect.z, 3.0)

    def test_convertsionString(self):
        vect = vector3.Vector3(0, 0, 0)
        vect.setX("1.254")
        self.assertEquals(vect.x, 1.254)
        vect.setY("2")
        self.assertEquals(vect.y, 2.0)
        vect.setZ("35200")
        self.assertEquals(vect.z, 35200.0)
