import os
import inspect
import unittest
from PyC3D import c3dReader, vector3

def getResource(resourceName):
    currentPath = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    resourcecesFolder = os.path.join(currentPath, "resources", "files")

    fileName = os.path.join(resourcecesFolder, resourceName)

    if os.path.isfile(fileName):
        return os.path.join(resourcecesFolder, resourceName)
    else:
        raise fileName

class C3DReaderBaseClass(unittest.TestCase):
    def setUp(self):
        self.reader = c3dReader.C3DReader()


class ConstructorTest(unittest.TestCase):
    def test_defaultValues(self):
        """ Test that the C3D Reader is constructed with the default values
            expected
        """
        reader = c3dReader.C3DReader()
        self.assertEqual(reader.getMarkerCount(), 0)
        self.assertEqual(reader.getMarkers(), [])
        self.assertEqual(reader.getFrameStart(), 0.0)
        self.assertEqual(reader.getFrameEnd(), 0.0)
        self.assertEqual(reader.getSampleRate(), 0.0)
        self.assertEqual(reader.listParamaters(), [])
        self.assertEqual(reader.getLoadedFilePath(), "")

    def test_fileLoadConstructor(self):
        """ Test if a file is passed into the constructor, the values are not
            the same as the default values
        """
        exampleFile = getResource("motion_A.c3d")
        reader = c3dReader.C3DReader(exampleFile)
        self.assertNotEqual(reader.getMarkerCount(), 0)
        self.assertNotEqual(reader.getMarkers(), [])
        self.assertNotEqual(reader.getFrameStart(), 0.0)
        self.assertNotEqual(reader.getFrameEnd(), 0.0)
        self.assertNotEqual(reader.getSampleRate(), 0.0)
        self.assertNotEqual(reader.listParamaters(), [])
        self.assertNotEqual(reader.getLoadedFilePath(), "")


class RealWorldTests(C3DReaderBaseClass):
    def test_wholeFileReadA(self):
        """ Read the whole C3D File and ensure the results are as expected
        """  #
        exampleFile = getResource("motion_A.c3d")
        self.reader.loadFile(exampleFile)
        internalDict = self.reader._C3DReader__paramaterDict
        expectedDict = {'TRIAL': {
                                'ACTUAL_START_FIELD': [267, 0],
                                'VIDEO_RATE_DIVIDER': 1,
                                'ACTUAL_END_FIELD': [1400, 0],
                                'CAMERA_RATE': 119.87999725341797
                                },
                        'SUBJECTS': {
                                    'IS_STATIC': 0,
                                    'USES_PREFIXES': 1,
                                    'LABEL_PREFIXES': ['Lloyd:'],
                                    'USED': 1,
                                    'NAMES': ['Lloyd']
                                    },
                    'EVENT_CONTEXT': {
                                    'COLOURS': [192, 0, 0, 0, 192, 0, 0, 0, 192],
                                    'LABELS': [
                                            'Left',
                                            'Right',
                                            'General'
                                            ],
                                    'USED': 3,
                                    'ICON_IDS': [0, 0, 0],
                                    'DESCRIPTIONS': [
                                                    'Left side',
                                                    'Right side',
                                                    'For other events'
                                                    ]
                                    },
                    'EVENT': {
                            'USED': 0
                            },
                    'POINT': {
                            'USED': 47,
                            'MOVIE_DELAY': 0.0,
                            'LABELS': [
                                    'Lloyd:Root',
                                    'Lloyd:LFWT',
                                    'Lloyd:RFWT',
                                    'Lloyd:LBWT',
                                    'Lloyd:RBWT',
                                    'Lloyd:LTHI',
                                    'Lloyd:LKNE',
                                    'Lloyd:LSHN',
                                    'Lloyd:LANK',
                                    'Lloyd:LHEE',
                                    'Lloyd:LTOE',
                                    'Lloyd:LMT5',
                                    'Lloyd:RTHI',
                                    'Lloyd:RKNE',
                                    'Lloyd:RSHN',
                                    'Lloyd:RANK',
                                    'Lloyd:RHEE',
                                    'Lloyd:RTOE',
                                    'Lloyd:RMT5',
                                    'Lloyd:TopSpine',
                                    'Lloyd:MidBack',
                                    'Lloyd:LowerBack',
                                    'Lloyd:Pelvis',
                                    'Lloyd:STRN',
                                    'Lloyd:LFTShould',
                                    'Lloyd:RFTShould',
                                    'Lloyd:LRRShould',
                                    'Lloyd:RRRShould',
                                    'Lloyd:LFHD',
                                    'Lloyd:RFHD',
                                    'Lloyd:LBHD',
                                    'Lloyd:RBHD',
                                    'Lloyd:ARIEL',
                                    'Lloyd:LSHO',
                                    'Lloyd:LUPA',
                                    'Lloyd:LELB',
                                    'Lloyd:LFRM',
                                    'Lloyd:LWRIST',
                                    'Lloyd:LTHUMB',
                                    'Lloyd:LPINKY',
                                    'Lloyd:RSHO',
                                    'Lloyd:RUPA',
                                    'Lloyd:RELB',
                                    'Lloyd:RFRM',
                                    'Lloyd:RWRIST',
                                    'Lloyd:RTHUMB',
                                    'Lloyd:RPINKY'
                                    ],
                            'DATA_START': 10,
                            'UNITS': 'mm',
                            'RATE': 119.87999725341797,
                            'SCALE':-0.10000000149011612,
                            'FRAMES': 1134
                            }
                    }
        for key in expectedDict.keys():
            self.assertEqual(internalDict[key], expectedDict[key])

        self.assertEqual(internalDict, expectedDict)

    def test_wholeFileReadB(self):
        """ Read the whole C3D File and ensure the results are as expected
        """  #
        exampleFile = getResource("motion_B.c3d")
        self.reader.loadFile(exampleFile)
        internalDict = self.reader._C3DReader__paramaterDict
        expectedDict = {'TRIAL': {
                                'ACTUAL_END_FIELD': [1136, 0],
                                'ACTUAL_START_FIELD': [992, 0],
                                'CAMERA_RATE': 119.87999725341797,
                                'VIDEO_RATE_DIVIDER': 1
                                },
                        'SUBJECTS': {
                                    'IS_STATIC': 0,
                                    'USES_PREFIXES': 1,
                                    'LABEL_PREFIXES': ['Lloyd:'],
                                    'USED': 1,
                                    'NAMES': ['Lloyd']
                                    },
                    'EVENT_CONTEXT': {
                                    'COLOURS': [192, 0, 0, 0, 192, 0, 0, 0, 192],
                                    'LABELS': [
                                            'Left',
                                            'Right',
                                            'General'
                                            ],
                                    'USED': 3,
                                    'ICON_IDS': [0, 0, 0],
                                    'DESCRIPTIONS': [
                                                    'Left side',
                                                    'Right side',
                                                    'For other events'
                                                    ]
                                    },
                    'EVENT': {
                            'USED': 0
                            },
                    'POINT': {
                            'USED': 47,
                            'MOVIE_DELAY': 0.0,
                            'LABELS': [
                                    'Lloyd:Root',
                                    'Lloyd:LFWT',
                                    'Lloyd:RFWT',
                                    'Lloyd:LBWT',
                                    'Lloyd:RBWT',
                                    'Lloyd:LTHI',
                                    'Lloyd:LKNE',
                                    'Lloyd:LSHN',
                                    'Lloyd:LANK',
                                    'Lloyd:LHEE',
                                    'Lloyd:LTOE',
                                    'Lloyd:LMT5',
                                    'Lloyd:RTHI',
                                    'Lloyd:RKNE',
                                    'Lloyd:RSHN',
                                    'Lloyd:RANK',
                                    'Lloyd:RHEE',
                                    'Lloyd:RTOE',
                                    'Lloyd:RMT5',
                                    'Lloyd:TopSpine',
                                    'Lloyd:MidBack',
                                    'Lloyd:LowerBack',
                                    'Lloyd:Pelvis',
                                    'Lloyd:STRN',
                                    'Lloyd:LFTShould',
                                    'Lloyd:RFTShould',
                                    'Lloyd:LRRShould',
                                    'Lloyd:RRRShould',
                                    'Lloyd:LFHD',
                                    'Lloyd:RFHD',
                                    'Lloyd:LBHD',
                                    'Lloyd:RBHD',
                                    'Lloyd:ARIEL',
                                    'Lloyd:LSHO',
                                    'Lloyd:LUPA',
                                    'Lloyd:LELB',
                                    'Lloyd:LFRM',
                                    'Lloyd:LWRIST',
                                    'Lloyd:LTHUMB',
                                    'Lloyd:LPINKY',
                                    'Lloyd:RSHO',
                                    'Lloyd:RUPA',
                                    'Lloyd:RELB',
                                    'Lloyd:RFRM',
                                    'Lloyd:RWRIST',
                                    'Lloyd:RTHUMB',
                                    'Lloyd:RPINKY'
                                    ],
                            'DATA_START': 10,
                            'UNITS': 'mm',
                            'RATE': 119.87999725341797,
                            'SCALE':-0.10000000149011612,
                            'FRAMES': 145
                            }
                    }
        for key in expectedDict.keys():
            self.assertEqual(internalDict[key], expectedDict[key])

        self.assertEqual(internalDict, expectedDict)

class TestSetTwo(C3DReaderBaseClass):

    def test_defaultValues(self):
        # from nose.tools import set_trace; set_trace()
        pop = getResource("NotValidFile_A.c3d")
        n = 1
