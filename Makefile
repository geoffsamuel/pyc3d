# Makefile for PyMake, by Geoff Samuel

# Module Name		:	EnterNameHere
# Description		:	This is where you can write what the tool/module is used
# Author			:	Orignal Author's Name
# Co-Author			:	Any Co-Author's Name(s)
# Support			:	Where people should email for support
# Wedsite			:	Website location for this tool

version=2.0.0

target=PYTHON,OTHER

pythonSrc=python/
otherSrc=python/
otherTypes=xml,txt
convertPyQtUI=True
stringSubstute=0.5.0:$CALL(version), !TESTVAL!:this is a test value

centralDeploy=C:/pythonTools/deployed/$CALL(toolname)/versions/$CALL(versionToPy($CALL(version)))/

deployment=$CALL(fromEnv(PYMAKE_INSTALL))